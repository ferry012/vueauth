class Notification{

    success(){
        new Noty({
      type: 'success',
      layout: 'topRight',
      text: 'Successfully Done!',
      timeout: 2000,
         }).show();
    }

    update(){
        new Noty({
            type: 'success',
            layout: 'topRight',
            text: 'Successfully Updated!',
            timeout: 2000,
        }).show();
    }

    alert(){
        new Noty({
      type: 'alert',
      layout: 'topRight',
      text: 'Are you Sure?',
      timeout: 1000,
         }).show();
    }

    error(){
        new Noty({
      type: 'alert',
      layout: 'topRight',
      text: 'Something Went Wrong ! ',
      timeout: 1000,
         }).show();
    }


   warning(){
        new Noty({
      type: 'warning',
      layout: 'topRight',
      text: 'Something Went Wrong',
      timeout: 1000,
         }).show();
    }

    reservedError(){
        new Noty({
            type: 'warning',
            layout: 'topRight',
            text: 'No Products to be reserved',
            timeout: 1000,
        }).show();
    }
    successDelete(){
        new Noty({
            type: 'success',
            layout: 'topRight',
            text: 'Successfully Deleted!',
            timeout: 1000,
        }).show();
    }

    noCustomer(){
        new Noty({
            type: 'warning',
            layout: 'topRight',
            text: 'Please select customer',
            timeout: 1000,
        }).show();
    }

    duplicated(){
        new Noty({
            type: 'warning',
            layout: 'topRight',
            text: 'Duplicate Entry. Please try again',
            timeout: 2000,
        }).show();
    }

    existing(){
        new Noty({
            type: 'warning',
            layout: 'topRight',
            text: 'Existing Material. Please try again',
            timeout: 1000,
        }).show();
    }

    notFoundBarcode(){
        new Noty({
            type: 'warning',
            layout: 'topRight',
            text: 'Barcode Not Found',
            timeout: 1000,
        }).show();
    }

    image_validation(){
      new Noty({
      type: 'error',
      layout: 'topRight',
      text: 'Upload Image less then 50MB ',
      timeout: 1000,
         }).show();
    }



      cart_success(){
      new Noty({
      type: 'success',
      layout: 'topRight',
      text: 'Successfully Add to Cart!',
      timeout: 1000,
         }).show();
    }


     cart_delete(){
      new Noty({
      type: 'success',
      layout: 'topRight',
      text: 'Successfully Deleted!',
      timeout: 1000,
         }).show();
    }
    priceNotFound(){
        new Noty({
      type: 'warning',
      layout: 'topRight',
      text: 'Price not Found!',
      timeout: 1000,
         }).show();
    }

   marketData(){
        new Noty({
      type: 'warning',
      layout: 'topRight',
      text: 'Incorrect Data!',
      timeout: 3000,
         }).show();
    }


  }

  export default Notification = new Notification()
